Création d'une API de gestion de concession auto dans le cadre d'un exercice technique (Two-I).

## Énoncé

Mettre en place une API REST qui gère une concession de voitures.
Techno à utiliser : NodeJS, PostgreSQL / MySQL, Express
Les collections/tables sont les suivantes : CUSTOMER, CAR, ORDERS

## On doit pouvoir :
    * Créer, Lire, Modifier et Supprimer un client (CUSTOMER), une voiture (CAR) ou une commande (ORDERS)
    * Récupérer les commandes liées à un client en particulier
    * Récupérer la ou les voitures associées à une commande en particulier

## Bonus :
    * Système d'authentification

## Installation

    * Cloner le projet
    * Copier le script de création de BDD dans MySQL
    * Npm install
    * nodemon index.js
